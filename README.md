# symfony/http-foundation

Object-oriented layer for the HTTP specification https://symfony.com/http-foundation

https://phppackages.org/p/symfony/http-foundation
![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/http-foundation)

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=php-psr-http-message+php-psr-http-factory+php-fig-http-message-util+php-slim-psr7+php-guzzlehttp-psr7+php-symfony-http-foundation+php-http-message-factory+php-nyholm-psr7&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=php-symfony-filesystem+php-symfony-console+php-symfony-process+php-symfony-finder+php-symfony-expression-language+php-symfony-cache+php-symfony-config+php-symfony-dependency-injection+php-symfony-event-dispatcher&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=php-symfony-event-dispatcher+php-symfony-routing+php-symfony-http-foundation+php-symfony-http-kernel&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=1&want_legend=1&want_ticks=1&from_date=2014-01-01&to_date=&hlght_date=&date_fmt=%25Y)

[[_TOC_]]

# Official documentation
* [*The HttpFoundation Component*
  ](https://symfony.com/doc/current/components/http_foundation.html)
* [*New in Symfony 3.3: Cookie improvements*
  ](https://symfony.com/blog/new-in-symfony-3-3-cookie-improvements)
  2016-12 Javier Eguiluz


# Unofficial documentation
* [*Serve a file stream in Symfony*
  ](https://dev.to/rubenrubiob/serve-a-file-stream-in-symfony-3ei3)
  2023-09 Rubén Rubio
* [*Managing User Sessions in Symfony 6: A Beginner’s Guide PT2*
  ](https://medium.com/codex/managing-user-sessions-in-symfony-6-a-beginners-guide-pt2-f84d6ee158d6)
  2023-05 Nico Anastasio
* [*Managing User Sessions in Symfony 6: A Beginner’s Guide*
  ](https://faun.pub/managing-user-sessions-in-symfony-6-a-beginners-guide-64e9e12ab4c)
  Discover best practices for using sessions in Symfony 6 and how to avoid common pitfalls
  2023-04 Nico Anastasio
* [*Symfony Request tutorial*
  ](http://zetcode.com/symfony/request)
  2020-07 zetcode

# Differs from PSR-7
* [*Why I should abstract HTTP messages?*](https://medium.com/@paooolino/why-i-should-abstract-http-messages-bc971b7c2926)
  * [*PSR-7 is imminent, and here's my issues with it*](https://evertpot.com/psr-7-issues/)
  * [*Immutable object*](https://en.wikipedia.org/wiki/Immutable_object)

# Features: Cookies
* [Cookie.php
  ](https://github.com/symfony/symfony/blob/master/src/Symfony/Component/HttpFoundation/Cookie.php)

# Programming style
* [*PHP: Never type hint on arrays*
  ](https://steemit.com/php/@crell/php-never-type-hint-on-arrays)
  2018 crell
* [*Iterable objects and array type hinting?*
  ](https://stackoverflow.com/questions/3584700/iterable-objects-and-array-type-hinting)
  (2018)
